# Information / Информация

SPEC-файл для создания RPM-пакета **mc**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/mc`.
2. Установить пакет: `dnf install mc`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)